#fst-rstempak.py
import sys

class machine:
    def __init__(self, description):
        self.name = description[0].strip()
        self.input_alphabet = description[1].strip().split(",")
        self.output_alphabet = description[2].strip().split(",")
        self.states = description[3].strip().split(",")
        self.start_state = description[4].strip()
        self.accepting_states = description[5].strip().split(",")
        self.transition_function = {}

        # echo name to stdout
        print self.name

        # parse lines for transition_function (format = transition_function[state][transition] = (rule_number, reachable_state, output char)
        for state in self.states:
            self.transition_function[state] = {}

        rule_number = 0
        for line in description[6:]:
            if len(line.strip().split(",")) == 4:
                InitialStateName, InputSymbol, NewStateName, OutputSymbol = line.strip().split(",")
            else:
                InitialStateName, InputSymbol, NewStateName = line.strip().split(",")
                OutputSymbol = ""

            # echo rule to stdout
            print "Rule {}: {} --{}--> {} with output: {}".format(rule_number, InitialStateName, InputSymbol, NewStateName, OutputSymbol)

            # fill transition_function
            self.transition_function[InitialStateName][InputSymbol] = (rule_number, NewStateName, OutputSymbol)

            rule_number+=1

    def run_tests(self, test_cases):
        ### init final data variables ###
        num_accepted = 0
        num_rejected = 0

        for line in test_cases:
            string = line.strip()
            print "\nString:", string
            output_string = ""

            ### test string ###
            InitialStateName = self.start_state
            for Stepnumber, InputSymbol in enumerate(string):
                RuleNumber, NewStateName, OutputSymbol = self.transition_function[InitialStateName][InputSymbol]
                print "{},{},{},{},{},{}".format(Stepnumber,RuleNumber,InitialStateName,InputSymbol,NewStateName,OutputSymbol)
                InitialStateName = NewStateName
                output_string = output_string + OutputSymbol

            ### results ####
            if InitialStateName in self.accepting_states:
                print "Accepted"
                num_accepted+=1
            else:
                print "Rejected"
                num_rejected+=1

            print "Output String:", output_string

        ### final tallies ###
        print "Number Accepted: {}".format(num_accepted)
        print "Number Rejected: {}".format(num_rejected)

def main():
    args = sys.argv[1:]
    if len(sys.argv) != 3:
        print ("Usage: python oneD-define-rstempak.py <machine file> <test cases file>")
        return

    ### read machine description and test cases ###
    machine_file = args[0]
    test_cases_file = args[1]

    with open(machine_file) as machinef:
        machine_description = machinef.readlines()

    with open(test_cases_file) as testf:
        test_cases = testf.readlines()

    ### run tests ###
    my_machine = machine(machine_description)
    my_machine.run_tests(test_cases)

if __name__ == '__main__':
    main()
